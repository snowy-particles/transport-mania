using UnityEngine;
using UnityEngine.UIElements;

public class FactoryView : MonoBehaviour {

    void OnEnable() {
        // The UXML is already instantiated by the UIDocument component
        var uiDocument = GetComponent<UIDocument>();
        // Hide the factory GUI by default.
        uiDocument.enabled = false;
    }
}
