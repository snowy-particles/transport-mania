using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;

public class Interactable : MonoBehaviour, IPointerDownHandler {
    private void Start() {
        AddPhysicsRaycaster();
    }

    public void OnPointerDown(PointerEventData eventData) {
        GameObject clickedObject = eventData.pointerCurrentRaycast.gameObject;
        Debug.Log("Clicked: " + clickedObject.name);
        if (clickedObject != null) {
            switch (clickedObject.name) {
                case "factory1":
                    // When the player clicks on `factory1`, open its (for now placeholder) GUI menu.
                    var uiDocument = clickedObject.GetComponent<UIDocument>();
                    uiDocument.enabled = true;
                    break;
            }
        }

    }

    private void AddPhysicsRaycaster() {
        PhysicsRaycaster physicsRaycaster = FindObjectOfType<PhysicsRaycaster>();
        if (physicsRaycaster == null) {
            Camera.main.gameObject.AddComponent<PhysicsRaycaster>();
        }
    }
}
