using System;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    // variables
    const float movementSpeed = 100f;
    int currentZoom = 60;
    const int minZoom = 40;
    const int maxZoom = 70;
    const int yPos = 5;
    // position limits, goes both ways ex. if x is 10, min limit of x is -10, max is 10
    Vector2 mapSize = new Vector2(10, 10);

    void Start() {
        Camera.main.transform.position = new Vector3(0, yPos, -5);
    }

    void Update() {
        Vector3 cameraPos = Camera.main.transform.position;

        // moves camera opposite of the mouse direction
        if (Input.GetMouseButton(0)) {
            Vector3 newPos = Vector3.zero;
            newPos.x = -Input.GetAxis("Mouse X") * movementSpeed * Time.deltaTime;
            newPos.z = -Input.GetAxis("Mouse Y") * movementSpeed * Time.deltaTime;

            cameraPos += newPos;
        }

        // limits the camera's space to move around in
        if (cameraPos.x < mapSize.x || cameraPos.x > -mapSize.x
            || cameraPos.z < mapSize.y || cameraPos.z > -mapSize.y) {

            float x = Math.Clamp(cameraPos.x, -mapSize.x, mapSize.x);
            float z = Math.Clamp(cameraPos.z, -mapSize.y, mapSize.y);

            cameraPos = new Vector3(x, yPos, z);
        }

        // changes the field of view of camera aka zoom
        if (Input.mouseScrollDelta.y > 0) {
            currentZoom = Mathf.Clamp(currentZoom - 2, minZoom, maxZoom);
        } else if (Input.mouseScrollDelta.y < 0) {
            currentZoom = Mathf.Clamp(currentZoom + 2, minZoom, maxZoom);
        }

        // apply camera updates
        Camera.main.fieldOfView = currentZoom;
        Camera.main.transform.position = cameraPos;
    }
}
